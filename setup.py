from setuptools import find_packages, setup

package_name = 'rl_slam'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='jeff',
    maintainer_email='pflueger.j@northeastern.edu',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        		'rl_slam_runner = rl_slam.rl_slam_runner:main',
                'rl_slam_service = rl_slam.get_keyframe_service:main',
				],
    },
)
