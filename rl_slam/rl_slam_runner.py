# from stable_baselines3 import A2C
# from stable_baselines3.common.env_checker import check_env

import os

import matplotlib.pyplot as plt
import numpy as np
import torch
import torchvision
from torch.utils.tensorboard import SummaryWriter
from torchvision import datasets, transforms
from tqdm import tqdm
from .a2c import A2C

import gymnasium as gym

from rl_slam.rl_slam_env import RLSlamEnv
import rclpy

def main():
    rclpy.init()
    env = RLSlamEnv(pointcloud_topic="/ouster/points", imu_topic="/ouster/imu")
    action_shape = env.action_space.n
    obs_shape = env.observation_space.shape[0]

    writer = SummaryWriter()

    n_envs = 1
    n_updates = 20
    n_steps_per_update = 200

    use_cuda=False
    if use_cuda:
        device=torch.device("cuda" if torch.cuda_is_available() else "cpu")
    else:
        device=torch.device("cpu")
    
    # Hyperparams
    gamma = 0.999
    lam = 0.95
    ent_coef = 0.01
    actor_lr = 0.001
    critic_lr = 0.005
    num_exp = 5

    agent = None
    env_wrapper = gym.wrappers.RecordEpisodeStatistics(env, deque_size=n_envs*n_updates)

    critic_losses = []
    actor_losses = []
    entropies = []

    episode_returns = []
    entropies_all = []
    critic_losses_all = []
    actor_losses_all = []

    for i in range(num_exp):
        agent = A2C(obs_shape, action_shape, device, actor_lr, critic_lr, n_envs=n_envs)
        # writer.add_graph(agent)
        traj_rwds = []
        for sample_phase in tqdm(range(n_updates)):
            ep_value_preds = torch.zeros(n_steps_per_update, n_envs, device=device)
            ep_rewards = torch.zeros(n_steps_per_update, n_envs, device=device)
            ep_action_log_probs = torch.zeros(n_steps_per_update, n_envs, device=device)
            masks = torch.zeros(n_steps_per_update, n_envs, device=device)

            if sample_phase == 0:
                states, info = env_wrapper.reset()
            
            step = 0
            while(step < n_steps_per_update):
            # for step in range(n_steps_per_update):
                actions, action_log_probs, state_value_preds, entropy = agent.select_action(
                    states
                )
                states, rewards, terminated, truncated, infos = env_wrapper.step(
                    actions.cpu().numpy()
                )
                if not infos["dlio_initialized"]:
                    continue
                ep_value_preds[step] = torch.squeeze(state_value_preds)
                ep_rewards[step] = torch.tensor(rewards, device=device)
                ep_action_log_probs[step] = action_log_probs

                # add mask for the return calculation later;
                # for each env the mask is 1 if the episode is ongoing and 0 if it is terminated
                masks[step] = torch.tensor([not (truncated or terminated)])
                step += 1
                if terminated or truncated:
                    break
            
            # calculate the losses for actor and critic
            critic_loss, actor_loss = agent.get_losses(
                ep_rewards,
                ep_action_log_probs,
                ep_value_preds,
                entropy,
                masks,
                gamma,
                lam,
                ent_coef,
                device,
            )

            ep_rewards_np = ep_rewards.detach().cpu().numpy()
            ep_rewards_avg = np.sum(ep_rewards_np) / ep_rewards_np.shape[0]

            writer.add_scalar('critic_loss', critic_loss, sample_phase)
            writer.add_scalar('actor_loss', actor_loss, sample_phase)
            writer.add_scalar('Average episode reward', ep_rewards_avg, sample_phase)

            agent.update_parameters(critic_loss, actor_loss)

            #log the losses and entropy
            critic_losses.append(critic_loss.detach().cpu().numpy())
            actor_losses.append(actor_loss.detach().cpu().numpy())
            entropies.append(entropy.detach().mean().cpu().numpy())
            traj_rwds.append(np.sum(ep_rewards_np))
            print("Experiment: %d, Update: %d" % (i, sample_phase))
            print(traj_rwds)
            states, info = env_wrapper.reset()
        
        entropies_all.append(entropies)
        critic_losses_all.append(critic_losses)
        actor_losses_all.append(actor_losses)
        episode_returns.append(np.array(traj_rwds).flatten())

        entropies = []
        critic_losses = []
        actor_losses = []

        save_weights = True
        load_weights = False
        actor_weights_path = "weights/actor_weights%i.h5"%(i)
        critic_weights_path = "weights/critic_weights%i.h5"%(i)

        if not os.path.exists("weights"):
            os.mkdir("weights")

        """ save network weights """
        if save_weights:
            torch.save(agent.actor.state_dict(), actor_weights_path)
            torch.save(agent.critic.state_dict(), critic_weights_path)
        
        """ load network weights """
        if load_weights:
            agent = A2C(obs_shape, action_shape, device, critic_lr, actor_lr)
            agent.actor.load_state_dict(torch.load(actor_weights_path))
            agent.critic.load_state_dict(torch.load(critic_weights_path))   
            agent.actor.eval()
            agent.critic.eval()

        # model = A2C("MlpPolicy", env)
        # model.learn(total_timesteps=10000, progress_bar=True)
        # model.save("models/a2c_rl_slam")
        
        # del model

        

        # obs, _ = env.reset("/home/pflueger-nail/bagfiles/scout_bag_no_camera/", seed=None, options=None)
        # env.reinit_playback("/home/pflueger-nail/bagfiles/scout_bag_no_camera")

        # flag = True
        # count = 1
        # while(flag):
        #     a = input()
        #     if (a == "stop"):
        #         flag=False
        #         continue
        #     env.step(count%5 == 0)
        #     if count%100 == 0:  
        #         obs, _ = env.reset("/home/pflueger-nail/bagfiles/scout_bag_no_camera/", seed=None, options=None)
        #     count += 1

    """ plot the results """
        # %matplotlib inline
    randomize_domain = False
    rolling_length = 1
    fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(12, 5))
    fig.suptitle(
        f"Training plots for {agent.__class__.__name__} in the LunarLander-v2 environment \n \
                (n_envs={n_envs}, n_steps_per_update={n_steps_per_update}, randomize_domain={randomize_domain})"
    )

    entropies_all = np.array(entropies_all)
    critic_losses_all = np.array(critic_losses_all)
    actor_losses_all = np.array(actor_losses_all)
    print(episode_returns)
    episode_returns = np.array(episode_returns)

    print("episode returns:", episode_returns)
    print("entropies:", entropies_all)
    print("critic_losses:", critic_losses_all)
    print("actor_losses:", actor_losses_all)

    # episode return
    episode_avgs = np.sum(episode_returns, axis=0) / num_exp
    episode_maxs = np.max(episode_returns, axis=0)
    episode_mins = np.min(episode_returns, axis=0)
    axs[0][0].set_title("Episode Returns")
    axs[0][0].plot(
        np.arange(len(episode_avgs)),
        episode_avgs
    )
    axs[0][0].fill_between(
        np.arange(len(episode_avgs)),
        episode_maxs,
        episode_mins,
        facecolor='blue',
        alpha=0.2    
    )
    axs[0][0].set_xlabel("Number of episodes")

    # entropy
    entropy_avgs = np.sum(entropies_all, axis=0) / num_exp
    entropy_maxs = np.max(entropies_all, axis=0)
    entropy_mins = np.min(entropies_all, axis=0)
    axs[1][0].set_title("Entropy")
    axs[1][0].plot(
        np.arange(len(entropy_avgs)),
        entropy_avgs,
    )
    axs[1][0].fill_between(
        np.arange(len(entropy_avgs)),
        entropy_maxs,
        entropy_mins,
        facecolor='blue',
        alpha=0.2    
    )
    axs[1][0].set_xlabel("Number of updates")


    # critic loss
    critic_losses_avgs = np.sum(critic_losses_all, axis=0) / num_exp
    critic_losses_maxs = np.max(critic_losses_all, axis=0)
    critic_losses_mins = np.min(critic_losses_all, axis=0)
    axs[0][1].set_title("Critic Losses")
    axs[0][1].plot(
        np.arange(len(critic_losses_avgs)),
        critic_losses_avgs,
    )
    axs[0][1].fill_between(
        np.arange(len(critic_losses_avgs)),
        critic_losses_maxs,
        critic_losses_mins,
        facecolor='blue',
        alpha=0.2    
    )
    axs[0][1].set_xlabel("Number of updates")


    # actor loss
    actor_losses_avgs = np.sum(actor_losses_all, axis=0) / num_exp
    actor_losses_maxs = np.max(actor_losses_all, axis=0)
    actor_losses_mins = np.min(actor_losses_all, axis=0)
    axs[1][1].set_title("Actor Losses")
    axs[1][1].plot(
        np.arange(len(actor_losses_avgs)),
        actor_losses_avgs,
    )
    axs[1][1].fill_between(
        np.arange(len(actor_losses_avgs)),
        actor_losses_maxs,
        actor_losses_mins,
        facecolor='blue',
        alpha=0.2    
    )
    axs[1][1].set_xlabel("Number of updates")

    avgs = np.sum(episode_returns, axis=1) / len(episode_returns)

    max_avg_return = np.max(avgs)
    index_max_avg = np.argmax(avgs)

    print("Max average return: %f"%(max_avg_return))
    print("Max average index: %d" % (index_max_avg))

    plt.tight_layout()
    plt.show()
    writer.close()

    env.shutdown()
    rclpy.shutdown()


if __name__ == "__main__":
    main()