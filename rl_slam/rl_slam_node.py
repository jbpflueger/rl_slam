import rclpy
from rclpy.node import Node
from rl_slam_interfaces.msg import ObservationMsg, KeyframeAction
from rl_slam_interfaces.srv import ReinitializeOdom, ReinitializeMap, Keyframe
import time

class Observation(object):
  def __init__(self):
    self.dd = 0.0
    self.theta_deg = 0.0
    self.num_nearby = 0
    self.total_num_keyframes = 0
    self.err_body = 0.0
    self.dlio_initialized = False
    self.latest_pc_id = None

class RLSlamSubscriber(Node):
    def __init__(self):
        super().__init__('rl_slam_subscriber')
        self.obs_topic = "observation"
        self.obs_sub = self.create_subscription(ObservationMsg,  self.obs_topic, self.obs_callback, 10)
        self.latest_obs = Observation()
    
    def obs_callback(self, msg):
        # self.get_logger().info("Received observation message.")
        self.latest_obs.dd = msg.dd
        self.latest_obs.theta_deg = msg.theta_deg
        self.latest_obs.num_nearby = msg.num_nearby
        self.latest_obs.total_num_keyframes = msg.total_num_keyframes
        self.latest_obs.err_body  = msg.err_body
        self.latest_obs.dlio_initialized = msg.dlio_initialized
        self.latest_obs.latest_pc_id = msg.pc_id

    def reinit_obs(self):
       self.latest_obs = Observation()


class RLSlamPublisher(Node):
    def __init__(self):
        super().__init__('rl_slam_publisher')
        self.action_topic = "rl_slam/keyframe_action"
        self.action_pub = self.create_publisher(KeyframeAction, self.action_topic, 10)
        self.reinit_odom_client = self.create_client(ReinitializeOdom, "reinit_dlio_odom")
        self.reinit_map_client = self.create_client(ReinitializeMap, "reinit_dlio_map")
        self.action_client = self.create_client(Keyframe, "get_keyframe_service")

    # def publish_action(self, action):
    #     self.get_logger().info("Publishing action: %r." % (bool(action)))
    #     keyframe_action = KeyframeAction()
    #     keyframe_action.should_keyframe = bool(action)
    #     self.action_pub.publish(keyframe_action)

    def publish_action(self, action):
       self.get_logger().info("Submitting action: %r." % (bool(action)))
       keyframe = Keyframe.Request()
       keyframe.action.should_keyframe = bool(action)
       action_future = self.action_client.call_async(keyframe)
       rclpy.spin_until_future_complete(self, action_future)
       return action_future.result()

    
    def reinit_dlio(self):
        self.get_logger().info("Reinitializing dlio")
        odom_req = ReinitializeOdom.Request()
        odom_response = self.reinit_odom_client.call_async(odom_req)
        rclpy.spin_until_future_complete(self, odom_response, timeout_sec=10)
        print("Odom Response: %r" % (odom_response.result().reinitialized))
        if (not odom_response.result().reinitialized):
            self.get_logger().error("Could not reinitialize odometry!")
            return False
        map_req = ReinitializeMap.Request()
        map_response = self.reinit_map_client.call_async(map_req)
        rclpy.spin_until_future_complete(self, map_response, timeout_sec=10)
        print("Map Response: %r" % (map_response.result().reinitialized))
        if (not map_response.result().reinitialized):
           self.get_logger().error("Could not reinitialize map!")
           return False
        return True

def main():
   rclpy.init()
   rl_slam_node = RLSlamNode()
   rclpy.spin(rl_slam_node)
   rclpy.shutdown()

if __name__ == "__main__":  
   main()