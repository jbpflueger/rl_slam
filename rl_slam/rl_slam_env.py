import numpy as np
import gymnasium as gym
from gymnasium import spaces
from rl_slam.playback_module import PlaybackModule
from rl_slam.rl_slam_node import RLSlamPublisher, RLSlamSubscriber, Observation
import rclpy
from threading import Thread
import time
import copy

NUM_ELAPSED_TIMESTEPS=1
WAIT_TIMEOUT = 1

class RLSlamEnv(gym.Env):
    def __init__(self, pointcloud_topic, imu_topic):
        super(RLSlamEnv, self).__init__()
        self.pointcloud_topic = pointcloud_topic
        self.imu_topic = imu_topic
        self._playback_module = PlaybackModule(pointcloud_topic, imu_topic)
        self._rl_slam_publisher = RLSlamPublisher()

        self._rl_slam_subscriber = RLSlamSubscriber()
        self.executor = rclpy.executors.SingleThreadedExecutor()
        # self.executor.add_node(self._rl_slam_subscriber)
        # self.et = Thread(target=self.executor.spin)
        # self.et.start()
        
        n_actions = 2
        self.action_space = spaces.Discrete(n_actions) 
        self.observation_space = spaces.Box(
            low=np.array([0.0, 0.0, 0.0]),
            high=np.array([np.inf, 360.0, np.inf]),
            dtype=np.float32
        )
        self.current_rollout_len = 0

    def reinit_dlio(self):
        reinit_success = self._rl_slam_publisher.reinit_dlio()
        if (not reinit_success):
            print("Could not reinit dlio. aborting reset.")
        return reinit_success
        
    def reinit_playback(self, bag_file_path):
        self._playback_module.set_topic_paths(self.pointcloud_topic, self.imu_topic)
        self._playback_module.begin_playback(bag_file_path)
        self._playback_module.step()

    def reinit_obs(self):
        self._rl_slam_subscriber.reinit_obs()
        self.latest_obs = self._rl_slam_subscriber.latest_obs
        self.prev_obs = Observation()
    
    # def reset(self, bag_file_path="/home/pflueger-nail/bagfiles/scout_bag_no_camera", start_timestamp = 0.0, seed=None, options=None):
    def reset(self, bag_file_path="/home/jeff/scout_bag_no_camera", start_timestamp = 0.0, seed=None, options=None):
        super().reset(seed=seed, options=options)
        self.latest_obs = Observation()
        self.prev_obs = Observation()
        self.current_rollout_len = 0
        reinit_success = self.reinit_dlio()
        time.sleep(1)
        self.reinit_obs()
        self.reinit_playback(bag_file_path)
        print("Obs: dd = %f; theta_deg = %f; num_nearby=%f; dlio_initalized = %r" % (
            self.latest_obs.dd,
            self.latest_obs.theta_deg,
            self.latest_obs.num_nearby,
            self.latest_obs.dlio_initialized,
        ))
        obs = np.array([
            self.latest_obs.dd,
            self.latest_obs.theta_deg,
            self.latest_obs.num_nearby,
        ]).astype(np.float32)
        return obs, {"reinit_success": reinit_success}

    def step(self, action):
        if (not self.latest_obs.dlio_initialized):
            print("DLIO NOT INITIALIZED")
            action = False
        elif (self.prev_obs.total_num_keyframes == 0):
            print("FIRST KEYFRAME NOT ADDED")
            action = False
        self._rl_slam_publisher.publish_action(action)
        self._playback_module.step()
        print(self.latest_obs.total_num_keyframes, self.prev_obs.total_num_keyframes)
        pc_id = self._playback_module.get_pc_id()
        # wait until we receive associated obs
        self.prev_obs = copy.deepcopy(self.latest_obs)
        while (pc_id and self._rl_slam_subscriber.latest_obs.latest_pc_id != pc_id):
            rclpy.spin_once(self._rl_slam_subscriber, executor=self.executor)
        self.latest_obs = self._rl_slam_subscriber.latest_obs
        print(self.latest_obs.total_num_keyframes, self.prev_obs.total_num_keyframes)
        print("Latest obs: dd = %f, theta_deg = %f, num_nearby = %d, dlio_initialized: %r, body_err: %f" % (
            self.latest_obs.dd,
            self.latest_obs.theta_deg,
            self.latest_obs.num_nearby,
            self.latest_obs.dlio_initialized,
            self.latest_obs.err_body
        ))
        obs = np.array([self.latest_obs.dd, self.latest_obs.theta_deg, self.latest_obs.num_nearby]).astype(np.float32)
        # TODO: Add termination/truncation conditions (timeouts/error too big or something)
        terminated = self.latest_obs.dd >= 100.0 or self.current_rollout_len > 200
        truncated = False
        # TODO: Add real reward calculation
        reward = self.reward(action)
        info = {"dlio_initialized": self.latest_obs.dlio_initialized}
        self.current_rollout_len += 1
        if self.latest_obs.total_num_keyframes > 50:
            truncated = True
        return (
           obs,
           reward,
           terminated,
           truncated,
           info
        )

    def reward(self, action: bool):
        return -self.latest_obs.err_body + -int(action) + NUM_ELAPSED_TIMESTEPS
    
    def shutdown(self):
        self.executor.shutdown()
        # self.et.join()