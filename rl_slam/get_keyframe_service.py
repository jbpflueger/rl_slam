import numpy as np
from stable_baselines3 import A2C
import torch

from rl_slam_interfaces.srv import GetKeyframeAction
from rl_slam_interfaces.msg import ObservationMsg, KeyframeAction
import rclpy
from rclpy.node import Node

KEYFRAME_THRESH_DIST = 1.0
KEYFRAME_THRESH_ROT = 45

def load_a2c_model(path: str) -> A2C:
    model = A2C.load(path)
    return model

class KeyframeService(Node):
  def __init__(self):
    super().__init__('keyframe_service')
    self.srv = self.create_service(GetKeyframeAction, 'get_keyframe_action', self.callback)
    self.latest_obs = ObservationMsg()
    self.model = load_a2c_model("/home/jeff/ros2_ws/models/a2c_rl_slam.zip")

  def reset(self):
    self.latest_obs = ObservationMsg()
    self.first_req = True
  
  def callback(self, request, response):
    self.get_logger().info("RECEIVED REQUEST")
    obs = np.array([request.obs.dd, request.obs.theta_deg, request.obs.num_nearby])
    out = self.model.predict(obs)
    new_keyframe = bool(out[0])
    response.action.should_keyframe = new_keyframe
    self.get_logger().info('Incoming request\n ShouldKeyframe = %r' % (response.action.should_keyframe))
    return response


def main():
	rclpy.init()
	keyframe_service = KeyframeService()
	rclpy.spin(keyframe_service)
	rclpy.shutdown()

if __name__ == "__main__":
	main()
