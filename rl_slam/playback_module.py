import argparse

import rosbag2_py
import rclpy
from rclpy.node import Node
from rclpy.serialization import deserialize_message
from rosidl_runtime_py.utilities import get_message
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu, PointCloud2

class PlaybackModule(Node):
    def __init__(self, pointcloud_topic: str, imu_topic: str):
        super().__init__('playback_module')
        self._pointcloud_topic = pointcloud_topic
        self._imu_topic = imu_topic
        # self._publisher_node = RLSLAMPublisherNode(pointcloud_topic, imu_topic)
        self._pointcloud_publisher = self.create_publisher(PointCloud2, pointcloud_topic, 10)
        self._imu_publisher = self.create_publisher(Imu, imu_topic, 100)
        self._reader = rosbag2_py.SequentialReader()
        self._in_playback = False
        self._current_topic_types = None
        self._next_pointcloud_msg = None
        self._latest_pc_id = None
    
    def publish_pointcloud(self, pointcloud_msg):
        self._pointcloud_publisher.publish(pointcloud_msg)
        # self.get_logger().info('Publishing pointcloud msg')

    def publish_imu(self, imu_msg):
        self._imu_publisher.publish(imu_msg)
        # self.get_logger().info('Publishing imu msg')

    def set_topic_paths(self, pointcloud_topic, imu_topic):
        self.destroy_publisher(self._pointcloud_publisher)
        self.destroy_publisher(self._imu_publisher)
        self._pointcloud_topic = pointcloud_topic
        self._pointcloud_publisher = self.create_publisher(PointCloud2, pointcloud_topic, 10)
        self._imu_topic = imu_topic
        self._imu_publisher = self.create_publisher(Imu, imu_topic, 100)
    
    def begin_playback(self, bag_file_path: str):
        self._reader.open(
            rosbag2_py.StorageOptions(uri=bag_file_path),
            rosbag2_py.ConverterOptions(
                input_serialization_format="cdr", output_serialization_format="cdr"
            )
        )
        self._in_playback = True
        self._current_topic_types = self._reader.get_all_topics_and_types()

    def typename(self, topic_name):
        for topic_type in self._current_topic_types:
            if topic_type.name == topic_name:
                return topic_type.type
        raise ValueError(f"topic {topic_name} not in bag")

    def step(self):
        if not self._in_playback:
            self.get_logger().error("Stepping while not in playback! Error")
            return None
        if not (self._next_pointcloud_msg == None):
            print("Point cloud msg: ", self._next_pointcloud_msg.header.stamp.sec*1000000000 + self._next_pointcloud_msg.header.stamp.nanosec)
            self._pointcloud_publisher.publish(self._next_pointcloud_msg)
            pc_id = self._next_pointcloud_msg.header.stamp
            self._latest_pc_id = pc_id
            self._next_pointcloud_msg = None
        # self.get_logger().info("Stepping to next pointcloud message")
        while self._next_pointcloud_msg == None:
            topic, data, timestamp = self._reader.read_next()
            msg_type = get_message(self.typename(topic))
            msg = deserialize_message(data, msg_type)
            if (isinstance(msg, Imu)):
                print("Imu msg: ", msg.header.stamp.sec*1000000000 + msg.header.stamp.nanosec)
                self._imu_publisher.publish(msg)
            if (isinstance(msg, PointCloud2)):
                self._next_pointcloud_msg = msg

    def end_of_log(self) -> bool:
        return not self._reader.has_next()
    
    def get_pc_id(self) -> int:
        return self._latest_pc_id


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "input", help="Input bag path to read from"
    )
    rclpy.init()
    args = parser.parse_args()
    pm = PlaybackModule("/ouster/points", "/ouster/imu")
    pm.begin_playback(args.input)
    while(True):
        pm.step()
    
if __name__ == "__main__":
    main()